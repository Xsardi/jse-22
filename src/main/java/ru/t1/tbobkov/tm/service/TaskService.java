package ru.t1.tbobkov.tm.service;

import ru.t1.tbobkov.tm.api.repository.ITaskRepository;
import ru.t1.tbobkov.tm.api.service.ITaskService;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.tbobkov.tm.exception.field.*;
import ru.t1.tbobkov.tm.exception.user.UserAccessDeniedException;
import ru.t1.tbobkov.tm.exception.user.UserIdEmptyException;
import ru.t1.tbobkov.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return repository.create(userId, name, description);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(userId, id);
        if (userId == null) throw new UserIdEmptyException();
        if (!userId.equals(task.getUserId())) throw new UserAccessDeniedException();
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(userId, index);
        if (userId == null) throw new UserIdEmptyException();
        if (!userId.equals(task.getUserId())) throw new UserAccessDeniedException();
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(userId, id);
        if (userId == null) throw new UserIdEmptyException();
        if (!userId.equals(task.getUserId())) throw new UserAccessDeniedException();
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        if (status == null) throw new StatusIncorrectException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (userId == null) throw new UserIdEmptyException();
        if (!userId.equals(task.getUserId())) throw new UserAccessDeniedException();
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

}
