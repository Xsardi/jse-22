package ru.t1.tbobkov.tm.service;

import ru.t1.tbobkov.tm.api.repository.IProjectRepository;
import ru.t1.tbobkov.tm.api.repository.ITaskRepository;
import ru.t1.tbobkov.tm.api.service.IProjectTaskService;
import ru.t1.tbobkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.tbobkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.tbobkov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.tbobkov.tm.exception.field.TaskIdEmptyException;
import ru.t1.tbobkov.tm.exception.user.UserAccessDeniedException;
import ru.t1.tbobkov.tm.exception.user.UserIdEmptyException;
import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        final Task task = taskRepository.findOneById(userId, taskId);
        final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        if (!userId.equals(task.getUserId())) throw new UserAccessDeniedException();
        task.setProjectId(null);
        return task;
    }

}
