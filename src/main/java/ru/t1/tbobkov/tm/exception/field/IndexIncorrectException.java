package ru.t1.tbobkov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Incorrect index....");
    }

}
