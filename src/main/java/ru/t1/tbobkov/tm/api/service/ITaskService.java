package ru.t1.tbobkov.tm.api.service;

import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    List<Task> findAllByProjectId(String userId, String id);

    Task create(String userId, String name, String description);

    Task create(String userId, String name);

}
