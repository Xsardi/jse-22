package ru.t1.tbobkov.tm.api.service;

import ru.t1.tbobkov.tm.api.repository.IUserOwnedRepository;
import ru.t1.tbobkov.tm.enumerated.Sort;
import ru.t1.tbobkov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

}
