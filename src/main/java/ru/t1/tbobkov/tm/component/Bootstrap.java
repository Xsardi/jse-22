package ru.t1.tbobkov.tm.component;

import ru.t1.tbobkov.tm.api.repository.ICommandRepository;
import ru.t1.tbobkov.tm.api.repository.IProjectRepository;
import ru.t1.tbobkov.tm.api.repository.ITaskRepository;
import ru.t1.tbobkov.tm.api.repository.IUserRepository;
import ru.t1.tbobkov.tm.api.service.*;
import ru.t1.tbobkov.tm.command.AbstractCommand;
import ru.t1.tbobkov.tm.command.project.*;
import ru.t1.tbobkov.tm.command.system.*;
import ru.t1.tbobkov.tm.command.task.*;
import ru.t1.tbobkov.tm.command.user.*;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.tbobkov.tm.exception.system.CommandNotSupportedException;
import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.model.Task;
import ru.t1.tbobkov.tm.repository.CommandRepository;
import ru.t1.tbobkov.tm.repository.ProjectRepository;
import ru.t1.tbobkov.tm.repository.TaskRepository;
import ru.t1.tbobkov.tm.repository.UserRepository;
import ru.t1.tbobkov.tm.service.*;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserRegistryCommand());
        registry(new UserRegistryCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());


        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationSystemInfoCommand());
        registry(new ApplicationVersionCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.tst");
        userService.create("admin", "admin", Role.ADMIN);
        final String testUserId = userService.findByLogin("test").getId();

        projectService.add(testUserId, new Project("A_TEST_PROJ_1_PROGRESS", Status.IN_PROGRESS));
        projectService.add(testUserId, new Project("C_TEST_PROJ_2_NOTSTARTED", Status.NOT_STARTED));
        projectService.add(testUserId, new Project("B_TEST_PROJ_3_COMPLETED", Status.COMPLETED));
        projectService.add(testUserId, new Project("D_TEST_PROJ_4_NOTSTARTED", Status.NOT_STARTED));

        taskService.add(testUserId, new Task("C_TEST_TASK_1_COMPLETED", Status.COMPLETED));
        taskService.add(testUserId, new Task("B_TEST_TASK_2_NOTSTARTED", Status.NOT_STARTED));
        taskService.add(testUserId, new Task("D_TEST_TASK_3_NOTSTARTED", Status.NOT_STARTED));
        taskService.add(testUserId, new Task("A_TEST_TASK_4_INPROGRESS", Status.IN_PROGRESS));
    }

    private void processArgs(final String[] args) {
        if (args == null || args.length < 1) return;
        processArg(args[0]);
        exit();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.printf("\n");
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArg(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    private void exit() {
        System.exit(0);
    }

    private void initLogger() {
        loggerService.info("** TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN**");
            }
        });
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        initDemoData();
        initLogger();
        processArgs(args);
        processCommands();
    }

}