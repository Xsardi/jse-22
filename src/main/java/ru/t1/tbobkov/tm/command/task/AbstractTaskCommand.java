package ru.t1.tbobkov.tm.command.task;

import ru.t1.tbobkov.tm.api.service.IProjectService;
import ru.t1.tbobkov.tm.api.service.IProjectTaskService;
import ru.t1.tbobkov.tm.api.service.ITaskService;
import ru.t1.tbobkov.tm.command.AbstractCommand;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("PROJECT: " + getProjectName(task));
        System.out.println("CREATED: " + task.getCreated());
    }

    private String getProjectName(final Task task) {
        if (task == null) return "";
        final String projectId = task.getProjectId();
        if (projectId == null || projectId.isEmpty()) return "";
        final Project project = getProjectService().findOneById(projectId);
        if (project == null) return "";
        return project.getName();
    }

}
