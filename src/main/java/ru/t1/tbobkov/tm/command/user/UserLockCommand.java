package ru.t1.tbobkov.tm.command.user;

import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    private static final String NAME = "user-lock";

    private static final String DESCRIPTION = "lock user";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
