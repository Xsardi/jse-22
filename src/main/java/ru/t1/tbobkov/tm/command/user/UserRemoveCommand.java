package ru.t1.tbobkov.tm.command.user;

import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    private static final String NAME = "user-remove";

    private static final String DESCRIPTION = "remove user";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
