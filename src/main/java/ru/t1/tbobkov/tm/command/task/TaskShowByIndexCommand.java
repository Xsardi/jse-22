package ru.t1.tbobkov.tm.command.task;

import ru.t1.tbobkov.tm.model.Task;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    private static final String NAME = "task-show-by-index";

    private static final String DESCRIPTION = "find task by index and show its data";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Task task = getTaskService().findOneByIndex(userId, index);
        if (task == null) return;
        showTask(task);
    }

}
