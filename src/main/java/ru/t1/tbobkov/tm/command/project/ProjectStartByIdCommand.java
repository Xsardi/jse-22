package ru.t1.tbobkov.tm.command.project;

import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-start-by-id";

    private static final String DESCRIPTION = "find project by id and change its status to 'In Progress'";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

}