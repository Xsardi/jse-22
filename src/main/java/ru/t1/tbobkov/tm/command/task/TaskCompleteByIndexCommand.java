package ru.t1.tbobkov.tm.command.task;

import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    private static final String NAME = "task-complete-by-index";

    private static final String DESCRIPTION = "find task by index and change its status to 'Completed'";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
    }

}