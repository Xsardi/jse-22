package ru.t1.tbobkov.tm.command.user;

import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private static final String NAME = "user-update";

    private static final String DESCRIPTION = "update profile of current user";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER PROFILE UPDATE]");
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().update(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
