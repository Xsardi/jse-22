package ru.t1.tbobkov.tm.command.task;

import ru.t1.tbobkov.tm.model.Task;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-show-by-id";

    private static final String DESCRIPTION = "find task by id and show its data";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        if (task == null) return;
        showTask(task);
    }

}
